#pragma once

#include <twist/stdlike/atomic.hpp>

#include <cstdint>

namespace solutions {

class ConditionVariable {
 public:
  // Mutex - BasicLockable
  // https://en.cppreference.com/w/cpp/named_req/BasicLockable
  template <class Mutex>
  void Wait(Mutex& mutex) {
    notified_.store(0);

    mutex.unlock();

    notified_.FutexWait(0);

    mutex.lock();
  }

  void NotifyOne() {
    notified_.store(1);
    notified_.FutexWakeOne();
  }

  void NotifyAll() {
    notified_.store(1);
    notified_.FutexWakeAll();
  }

 private:
  twist::stdlike::atomic<uint32_t> notified_{0};
};

}  // namespace solutions
