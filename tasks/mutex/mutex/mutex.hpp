#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/util/spin_wait.hpp>

#include <cstdlib>

namespace solutions {

using twist::util::SpinWait;

class Mutex {
 public:
  void Lock() {
    while (true) {
      uint32_t unlocked = 0;

      if(locked_.compare_exchange_strong(unlocked, 1))
          break;

      locked_.FutexWait(true);
    }
  }

  void Unlock() {
    locked_.store(false);

    locked_.FutexWakeOne();
  }

 private:
  twist::stdlike::atomic<uint32_t> locked_{0};
};
}  // namespace solutions
