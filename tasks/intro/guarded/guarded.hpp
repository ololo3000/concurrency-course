#pragma once

#include <twist/stdlike/mutex.hpp>

namespace solutions {

// Automagically wraps all accesses to guarded object to critical sections
// Look at unit tests for API and usage examples
template <typename T>
class Guarded {
 public:
  // https://eli.thegreenplace.net/2014/perfect-forwarding-and-universal-references-in-c/
  template <typename... Args>
  Guarded(Args&&... args) : object_(std::forward<Args>(args)...) {
  }

  // Your code goes here
  // https://en.cppreference.com/w/cpp/language/operators
  class GuardedPointer {
   public:
    T* operator-> () {
      return ptr_;
    }

    GuardedPointer(T* ptr, twist::stdlike::mutex& mutex) : ptr_(ptr), lock_(mutex) {}

   private:
    T* ptr_;
    std::lock_guard<twist::stdlike::mutex> lock_;

    GuardedPointer(GuardedPointer& other) = delete;
    GuardedPointer(GuardedPointer&& other) = delete;
  };

  GuardedPointer operator-> () {
    return Guarded::GuardedPointer(&object_, mutex_);
  }

 private:
  T object_;
  twist::stdlike::mutex mutex_;  // Guards access to object_
};

}  // namespace solutions
